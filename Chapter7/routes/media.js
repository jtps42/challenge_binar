const express = require("express");
const router = express.Router();
const controller = require("../controllers/index");
const storage = require("../utils/media/storage");

//upload gambar
router.post(
  "/image",
  storage.image.single("media"),
  controller.uploadMedia.uploadSingle
);

//upload multiple picture
router.post(
  "/images",
  storage.image.array("media"),
  controller.uploadMedia.uploadMultiple
);

//upload single video
router.post(
  "/video",
  storage.video.single("video"),
  controller.uploadMedia.uploadSingleVideo
);

router.post(
  "/videos",
  storage.video.array("video"),
  controller.uploadMedia.uploadMultipleVideo
);

module.exports = router;
